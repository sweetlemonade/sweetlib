package org.sweetlemonade.sweetlib.events;

/**
 * Feb 3, 2015
 *
 * @author denis.mirochnik
 */
public interface OnGlobalEventListener
{
	void onGlobalEvent(Event event);
}
