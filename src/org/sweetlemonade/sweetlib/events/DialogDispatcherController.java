package org.sweetlemonade.sweetlib.events;

import static org.sweetlemonade.sweetlib.common.Checks.as;
import static org.sweetlemonade.sweetlib.common.Checks.isMainThread;
import static org.sweetlemonade.sweetlib.common.Checks.requireNotNull;

import java.util.Collections;

import org.sweetlemonade.sweetlib.app.SweetFragmentDialog;
import org.sweetlemonade.sweetlib.app.SweetHandler;
import org.sweetlemonade.sweetlib.events.EventDispatcher.EventDispatcherClient;

import android.app.Dialog;
import android.support.v4.app.Fragment;

/**
 * Sep 26, 2013
 * 
 * @author denis.mirochnik
 */
public class DialogDispatcherController implements EventDispatcherClient<OnDialogEventListener>
{
	private final Dialog mSource;

	public DialogDispatcherController(Dialog source)
	{
		mSource = requireNotNull(source);
	}

	@Override
	public Event configureEvent(Event event)
	{
		return event;
	}

	@Override
	public Iterable<OnDialogEventListener> getTargets(Event event)
	{
		final SweetFragmentDialog fragmentDialog = as(mSource, SweetFragmentDialog.class);
		final Fragment fragment = fragmentDialog == null ? null : fragmentDialog.getOwnerFragment();

		OnDialogEventListener target = as(fragment, OnDialogEventListener.class);

		if (target == null)
		{
			target = as(mSource.getOwnerActivity(), OnDialogEventListener.class);
		}

		return target == null ? Collections.<OnDialogEventListener> emptyList() : Collections.singletonList(target);
	}

	@Override
	public void dispatchToTarget(final OnDialogEventListener target, final Event event)
	{
		if (isMainThread())
		{
			target.onDialogEvent(mSource, event);
		}
		else
		{
			SweetHandler.runOnUiThread(new Runnable()
			{

				@Override
				public void run()
				{
					target.onDialogEvent(mSource, event);
				}
			});
		}
	}
}
