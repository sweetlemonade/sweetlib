package org.sweetlemonade.sweetlib.events;

import android.app.Dialog;

/**
 * Sep 26, 2013
 * 
 * @author denis.mirochnik
 */
public interface OnDialogEventListener
{
	void onDialogEvent(Dialog dialog, Event event);
}
