package org.sweetlemonade.sweetlib.events;

public enum DialogEvents implements EventAction
{
	POSITIVE,
	NEGATIVE,
	NEUTRAL,
	SHOW,
	CANCEL,
	DISMISS,
	ITEM,
	MULTI_ITEM;
}