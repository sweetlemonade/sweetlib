package org.sweetlemonade.sweetlib.events;

import android.app.Service;

public interface OnServiceEventListener
{
	void onServiceEvent(Service service, Event event);
}