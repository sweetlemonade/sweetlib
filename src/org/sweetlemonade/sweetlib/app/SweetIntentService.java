package org.sweetlemonade.sweetlib.app;

import android.app.IntentService;

/**
 * Sep 23, 2013
 * 
 * @author denis.mirochnik
 */
public abstract class SweetIntentService extends IntentService
{
	public SweetIntentService(String name)
	{
		super(name);
	}
}
