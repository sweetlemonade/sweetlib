package org.sweetlemonade.sweetlib.app;

import org.sweetlemonade.sweetlib.async.Async;

import android.app.Application;

/**
 * Jan 26, 2015
 *
 * @author denis.mirochnik
 */
public abstract class SweetApplication extends Application
{
	private static volatile SweetApplication sInstance;

	public SweetApplication()
	{
		sInstance = this;
	}

	@Override
	public final void onCreate()
	{
		super.onCreate();

		onApplicationCreate();

		new Async<Void>()
		{

			@Override
			protected Void doWork()
			{
				onAsyncCreate();

				return null;
			}
		};
	}

	public abstract void onApplicationCreate();

	public abstract void onAsyncCreate();

	public static SweetApplication get()
	{
		return sInstance;
	}
}
