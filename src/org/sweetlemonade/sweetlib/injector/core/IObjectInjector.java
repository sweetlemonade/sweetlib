package org.sweetlemonade.sweetlib.injector.core;

import org.sweetlemonade.sweetlib.injector.IInjector;

public interface IObjectInjector
{
	void inject();

	void deinject();

	public static class InjectorWrapper implements IObjectInjector
	{
		private final IInjector<?> mInjector;

		public InjectorWrapper(IInjector<?> injector)
		{
			mInjector = injector;
		}

		@Override
		public void inject()
		{
			mInjector.inject();
		}

		@Override
		public void deinject()
		{
			mInjector.deinject();
		}
	}
}