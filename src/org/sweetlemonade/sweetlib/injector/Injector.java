package org.sweetlemonade.sweetlib.injector;

import static org.sweetlemonade.sweetlib.common.Checks.requireNotNull;

import org.sweetlemonade.sweetlib.injector.core.FieldCollector;
import org.sweetlemonade.sweetlib.injector.core.FieldCollector.FieldsChecker;
import org.sweetlemonade.sweetlib.injector.core.FieldCollector.FieldsProcessor;

/**
 * Jan 27, 2015
 *
 * @author denis.mirochnik
 */
public abstract class Injector<T, P> implements FieldsProcessor<T>, IInjector<T>
{
	private final FieldCollector<T> mCollector;
	private final P mProvider;
	private final Object mTarget;

	private boolean mInjecting;

	public Injector(P provider, Object target)
	{
		mProvider = requireNotNull(provider);
		mTarget = requireNotNull(target);

		mCollector = new FieldCollector<>(getChecker(), this);
		mCollector.collectFields(target);
	}

	@Override
	public void inject()
	{
		mInjecting = true;
		mCollector.processFields();
	}

	@Override
	public void deinject()
	{
		mInjecting = false;
		mCollector.processFields();
	}

	protected FieldCollector<T> getCollector()
	{
		return mCollector;
	}

	protected Object getTarget()
	{
		return mTarget;
	}

	protected P getProvider()
	{
		return mProvider;
	}

	protected boolean isInjecting()
	{
		return mInjecting;
	}

	protected abstract FieldsChecker<T> getChecker();
}
